package com.dhara.weatherapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.dhara.weatherapp.R;
import com.dhara.weatherapp.WeatherApp;
import com.dhara.weatherapp.adapters.viewholder.ForecastViewholder;
import com.dhara.weatherapp.constants.Global;
import com.dhara.weatherapp.interfaces.OnItemClickListener;
import com.dhara.weatherapp.models.WeatherData;
import com.dhara.weatherapp.utils.CommonUtils;
import com.dhara.weatherapp.utils.DateUtils;
import com.dhara.weatherapp.utils.SortUtils;
import com.dhara.weatherapp.utils.WeatherForecastUtils;
import com.dhara.weatherapp.utils.network.RestClient;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Dhara Shah on 16-05-2016. </br>
 * Adapter that displays individual weather reports for the next days
 */
public class ForecastAdapter extends RecyclerView.Adapter<ForecastViewholder> {
    private List<WeatherData> mForecastList;
    private OnItemClickListener mListener;

    public ForecastAdapter(OnItemClickListener listener) {
        mListener = listener;
    }

    @Override
    public ForecastViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.individual_single_forecast, parent, false);
        return new ForecastViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(ForecastViewholder holder, int position) {
        holder.txtWeatherDesc.setText(mForecastList.get(position).getWeather().get(0).getDescription());
        holder.txtWeatherDeg.setText(WeatherForecastUtils.getTemperature(mForecastList.get(position).getMain()));
        holder.txtDate.setText(DateUtils.getDay(mForecastList.get(position).getStrDate()).toUpperCase());

        Glide.with(WeatherApp.getAppContext())
                .load(WeatherForecastUtils.getImageIconURL(mForecastList.get(position).getWeather().get(0).getIcon()))
                .centerCrop()
                .crossFade()
                .into(holder.imgWeatherIcon);

        holder.bind(mForecastList.get(position), mListener);
    }

    @Override
    public int getItemCount() {
        return mForecastList.size();
    }

    public void setForecastList(List<WeatherData> weatherDataList) {
        mForecastList = weatherDataList;
        SortUtils.getInstance().sortObjects(mForecastList,
                SortUtils.SortUtilsEnum.SORT_DATE_WISE);
    }
}
