package com.dhara.weatherapp.adapters.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dhara.weatherapp.R;
import com.dhara.weatherapp.interfaces.OnItemClickListener;
import com.dhara.weatherapp.models.WeatherData;

/**
 * Created by Dhara Shah on 16-05-2016.
 */
public class ForecastViewholder extends RecyclerView.ViewHolder{
    public TextView txtDate;
    public TextView txtWeatherDesc;
    public TextView txtWeatherDeg;
    public LinearLayout lnrRoot;
    public ImageView imgWeatherIcon;

    public ForecastViewholder(View itemView) {
        super(itemView);
        lnrRoot = (LinearLayout)itemView.findViewById(R.id.lnr_root);
        txtDate = (TextView)itemView.findViewById(R.id.txt_day);
        txtWeatherDeg = (TextView)itemView.findViewById(R.id.txt_forecast_degrees);
        txtWeatherDesc = (TextView)itemView.findViewById(R.id.txt_forecast_desc);
        imgWeatherIcon = (ImageView)itemView.findViewById(R.id.img_weather_icon);
    }

    public void bind(final WeatherData data, final OnItemClickListener listener) {
        lnrRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null) listener.onItemClick(data);
            }
        });
    }
}
