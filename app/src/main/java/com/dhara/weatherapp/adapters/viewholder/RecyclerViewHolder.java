package com.dhara.weatherapp.adapters.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dhara.weatherapp.R;

/**
 * Created by OPTLPTP125 on 18-05-2016.
 */
public class RecyclerViewHolder extends RecyclerView.ViewHolder{
    public TextView txtTemp;
    public TextView txtWeatherDesc;
    public TextView txtHumidity;
    public TextView txtPressure;
    public TextView txtWind;
    public TextView txtDay;
    public TextView txtTime;
    public LinearLayout lnrHumidity;
    public LinearLayout lnrPressure;
    public LinearLayout lnrWind;
    public ImageView imgIcon;

    public RecyclerViewHolder(View itemView) {
        super(itemView);
        txtTemp = (TextView)itemView.findViewById(R.id.txt_temp);
        txtWeatherDesc = (TextView)itemView.findViewById(R.id.txt_weather_description);
        txtHumidity = (TextView)itemView.findViewById(R.id.txt_humidity);
        txtPressure = (TextView)itemView.findViewById(R.id.txt_pressure);
        txtWind = (TextView)itemView.findViewById(R.id.txt_wind);
        txtDay = (TextView)itemView.findViewById(R.id.txt_day);
        txtTime = (TextView)itemView.findViewById(R.id.txt_time);
        lnrHumidity = (LinearLayout) itemView.findViewById(R.id.lnr_humidity);
        lnrPressure = (LinearLayout) itemView.findViewById(R.id.lnr_pressure);
        lnrWind = (LinearLayout) itemView.findViewById(R.id.lnr_wind);
        imgIcon = (ImageView) itemView.findViewById(R.id.img_icon);
    }
}
