package com.dhara.weatherapp.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dhara.weatherapp.R;
import com.dhara.weatherapp.db.CityContract;

/**
 * Created by Dhara Shah on 17-05-2016. </br>
 * City cursor adapter for the city list
 */
public class CityCursorAdapter extends CursorAdapter {
    public CityCursorAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.individual_city_row, parent, false);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView txtCiyName = (TextView)view.findViewById(R.id.txtCityName);
        txtCiyName.setText(convertToString(cursor));
    }

    @Override
    public CharSequence convertToString(Cursor cursor) {
        String cityName = cursor.getString(cursor.getColumnIndex(CityContract.CityEntry.COLUMN_CITY_NAME));
        String countryCode = cursor.getString(cursor.getColumnIndex(CityContract.CityEntry.COLUMN_COUNTRY_CODE));

        return cityName + "," + countryCode;
    }
}
