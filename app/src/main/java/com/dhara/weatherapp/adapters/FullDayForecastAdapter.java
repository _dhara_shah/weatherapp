package com.dhara.weatherapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.dhara.weatherapp.R;
import com.dhara.weatherapp.WeatherApp;
import com.dhara.weatherapp.adapters.viewholder.RecyclerViewHolder;
import com.dhara.weatherapp.models.WeatherData;
import com.dhara.weatherapp.utils.DateUtils;
import com.dhara.weatherapp.utils.SortUtils;
import com.dhara.weatherapp.utils.WeatherForecastUtils;

import java.util.Collections;
import java.util.List;

/**
 * Created by Dhara Shah on 17-05-2016. </br>
 * Adapter for displaying the forecast details of a day
 */
public class FullDayForecastAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    private List<WeatherData> mWeatherDataList;

    public FullDayForecastAdapter(List<WeatherData> weatherDataList) {
        mWeatherDataList = weatherDataList;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.individual_weather_forecast, parent, false);
        return new RecyclerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.txtDay.setText(DateUtils.getDay(mWeatherDataList.get(position).getStrDate()));
        holder.txtTime.setText(DateUtils.getTime(mWeatherDataList.get(position).getStrDate()));
        holder.txtWeatherDesc.setText(mWeatherDataList.get(position).getWeather().get(0).getDescription());
        holder.txtTemp.setText(WeatherForecastUtils.getTemperature(mWeatherDataList.get(0).getMain()));
        holder.txtWind.setText(WeatherForecastUtils.getWind(mWeatherDataList.get(position).getWind()));

        holder.txtHumidity.setText(WeatherForecastUtils.getHumidity(mWeatherDataList.get(position).getMain()));
        holder.txtPressure.setText(WeatherForecastUtils.getPressure(mWeatherDataList.get(position).getMain()));

        holder.lnrHumidity.setVisibility(View.VISIBLE);
        holder.lnrPressure.setVisibility(View.VISIBLE);

        if(mWeatherDataList.get(position).getWind() != null) {
            holder.lnrWind.setVisibility(View.VISIBLE);
            holder.txtWind.setText(WeatherForecastUtils.getWind(mWeatherDataList.get(position).getWind()));
        }

        Glide.with(WeatherApp.getAppContext())
                .load(WeatherForecastUtils.getImageIconURL(mWeatherDataList.get(position).getWeather().get(0).getIcon()))
                .centerCrop()
                .crossFade()
                .into(holder.imgIcon);
    }

    @Override
    public int getItemCount() {
        return mWeatherDataList.size();
    }

    public void setList(List<WeatherData> weatherDataList) {
        mWeatherDataList = weatherDataList;
        SortUtils.getInstance().sortObjects(mWeatherDataList,
                SortUtils.SortUtilsEnum.SORT_DATE_WISE);
    }
}
