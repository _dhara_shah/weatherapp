package com.dhara.weatherapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Dhara Shah on 16-05-2016.
 */
public class MainWeather implements Parcelable{
    @SerializedName("temp")
    private double temperature;
    @SerializedName("temp_min")
    private float minTemp;
    @SerializedName("temp_max")
    private float maxTemp;
    @SerializedName("pressure")
    private float pressure;
    @SerializedName("sea_level")
    private float seaLevel;
    @SerializedName("grnd_level")
    private float groundLevel;
    @SerializedName("humidity")
    private float humidity;

    protected MainWeather(Parcel in) {
        temperature = in.readDouble();
        minTemp = in.readFloat();
        maxTemp = in.readFloat();
        pressure = in.readFloat();
        seaLevel = in.readFloat();
        groundLevel = in.readFloat();
        humidity = in.readFloat();
    }

    public static final Creator<MainWeather> CREATOR = new Creator<MainWeather>() {
        @Override
        public MainWeather createFromParcel(Parcel in) {
            return new MainWeather(in);
        }

        @Override
        public MainWeather[] newArray(int size) {
            return new MainWeather[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(temperature);
        dest.writeFloat(minTemp);
        dest.writeFloat(maxTemp);
        dest.writeFloat(pressure);
        dest.writeFloat(seaLevel);
        dest.writeFloat(groundLevel);
        dest.writeFloat(humidity);
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public float getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(float minTemp) {
        this.minTemp = minTemp;
    }

    public float getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(float maxTemp) {
        this.maxTemp = maxTemp;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    public float getSeaLevel() {
        return seaLevel;
    }

    public void setSeaLevel(float seaLevel) {
        this.seaLevel = seaLevel;
    }

    public float getGroundLevel() {
        return groundLevel;
    }

    public void setGroundLevel(float groundLevel) {
        this.groundLevel = groundLevel;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }
}
