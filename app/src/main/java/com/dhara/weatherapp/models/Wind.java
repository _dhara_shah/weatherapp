package com.dhara.weatherapp.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Dhara Shah on 16-05-2016.
 */
public class Wind implements Parcelable {
    private float speed;
    private float deg;

    protected Wind(Parcel in) {
        speed = in.readFloat();
        deg = in.readFloat();
    }

    public static final Creator<Wind> CREATOR = new Creator<Wind>() {
        @Override
        public Wind createFromParcel(Parcel in) {
            return new Wind(in);
        }

        @Override
        public Wind[] newArray(int size) {
            return new Wind[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(speed);
        dest.writeFloat(deg);
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getDeg() {
        return deg;
    }

    public void setDeg(float deg) {
        this.deg = deg;
    }
}
