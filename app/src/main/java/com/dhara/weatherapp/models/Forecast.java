package com.dhara.weatherapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Dhara Shah on 16-05-2016.
 */
public class Forecast {
    @SerializedName("cod")
    private String statusCode;
    @SerializedName("cnt")
    private int recordCount;
    private City city;
    @SerializedName("list")
    private List<WeatherData> forecastList;
    private List<WeatherData> groupedForecastList;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public int getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(int recordCount) {
        this.recordCount = recordCount;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public List<WeatherData> getForecastList() {
        return forecastList;
    }

    public void setForecastList(List<WeatherData> forecastList) {
        this.forecastList = forecastList;
    }

    public List<WeatherData> getGroupedForecastList() {
        return groupedForecastList;
    }

    public void setGroupedForecastList(List<WeatherData> groupedForecastList) {
        this.groupedForecastList = groupedForecastList;
    }
}
