package com.dhara.weatherapp.models;

/**
 * Created by Dhara Shah on 16-05-2016.
 */
public class Clouds {
    private int all;

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }
}
