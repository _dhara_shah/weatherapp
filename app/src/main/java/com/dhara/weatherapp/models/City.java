package com.dhara.weatherapp.models;

/**
 * Created by Dhara Shah on 16-05-2016.
 */
public class City {
    private long id;
    private String name;
    private String country;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
