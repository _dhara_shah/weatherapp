package com.dhara.weatherapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dhara Shah on 16-05-2016.
 */
public class WeatherData implements Parcelable{
    private long dt;
    private MainWeather main;
    private List<WeatherDetail> weather;
    @SerializedName("dt_txt")
    private String strDate;
    private Wind wind;

    protected WeatherData(Parcel in) {
        dt = in.readLong();
        main = in.readParcelable(MainWeather.class.getClassLoader());
        weather = new ArrayList<>();
        in.readTypedList(weather,WeatherDetail.CREATOR);
        strDate = in.readString();
        wind = in.readParcelable(Wind.class.getClassLoader());
    }

    public static final Creator<WeatherData> CREATOR = new Creator<WeatherData>() {
        @Override
        public WeatherData createFromParcel(Parcel in) {
            return new WeatherData(in);
        }

        @Override
        public WeatherData[] newArray(int size) {
            return new WeatherData[size];
        }
    };

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    public MainWeather getMain() {
        return main;
    }

    public void setMain(MainWeather main) {
        this.main = main;
    }

    public List<WeatherDetail> getWeather() {
        return weather;
    }

    public void setWeather(List<WeatherDetail> weather) {
        this.weather = weather;
    }

    public String getStrDate() {
        return strDate;
    }

    public void setStrDate(String strDate) {
        this.strDate = strDate;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(dt);
        dest.writeParcelable(main, flags);
        dest.writeList(weather);
        dest.writeString(strDate);
        dest.writeParcelable(wind, flags);
    }
}