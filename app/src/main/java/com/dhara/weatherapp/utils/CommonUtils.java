package com.dhara.weatherapp.utils;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.dhara.weatherapp.WeatherApp;

/**
 * Created by Dhara Shah on 17-05-2016.
 */
public class CommonUtils {
    /**
     * Hides the keyboard
     * @param view
     */
    public static void hideKeyboard(View view){
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)
                    WeatherApp.getAppContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
