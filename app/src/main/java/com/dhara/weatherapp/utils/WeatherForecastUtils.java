package com.dhara.weatherapp.utils;

import com.dhara.weatherapp.R;
import com.dhara.weatherapp.WeatherApp;
import com.dhara.weatherapp.constants.Global;
import com.dhara.weatherapp.models.MainWeather;
import com.dhara.weatherapp.models.WeatherData;
import com.dhara.weatherapp.models.Wind;
import com.dhara.weatherapp.utils.searchmanager.DateWiseWeatherPredicate;
import com.google.common.collect.Collections2;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

/**
 * Created by Dhara Shah on 17-05-2016. </br>
 * WeatherForecastUtils is a utility class for all weather related operations
 */
public class WeatherForecastUtils {
    /**
     * Wind directions that are used everywhere
     */
    private static final String[] windDirectionArr =
            new String[]{"N","NNE","NE","ENE","E","ESE", "SE", "SSE","S","SSW","SW","WSW","W","WNW","NW","NNW"};

    /**
     * Gets the list of weather forecast data (a single record per day)
     * @param forecastList
     * @return
     */
    public static List<WeatherData> getWeatherList(List<WeatherData> forecastList) {
        String currentDate = DateUtils.getCurrentDate();

        // now pick only those that do not have the current date
        Collection<WeatherData> collections = Collections2.filter(forecastList,
                new DateWiseWeatherPredicate(currentDate, false));

        List<WeatherData> groupedWeatherList = new ArrayList<>();

        if(collections != null) {
            List<WeatherData> groupedWeather = new ArrayList<>();
            groupedWeather.addAll(collections);

            // pick only the first of each since that would be the max temperature
            List<String> dateValues = new ArrayList<>();

            for(WeatherData data : groupedWeather) {
                if(!dateValues.contains(DateUtils.getDate(data.getStrDate()))) {
                    groupedWeatherList.add(data);
                    dateValues.add(DateUtils.getDate(data.getStrDate()));
                }
            }
        }

        return groupedWeatherList;
    }

    /**
     * Gets the current weather forecast as per the current time
     * @param forecastList
     * @return
     */
    public static WeatherData getCurrentWeather(List<WeatherData> forecastList) {
        String currentDate = DateUtils.getCurrentDate();
        String currentHour = DateUtils.getCurrentHour();

        // this list will contain the current day's weather forecast
        Collection<WeatherData> collections = Collections2.filter(forecastList,
                new DateWiseWeatherPredicate(currentDate, true));

        // use the current time, and get the weather data
        // that needs to be displayed for the current hour
        List<WeatherData> weatherDataList = new ArrayList<>();
        weatherDataList.addAll(collections);

        int prevHour = 0;
        int currRecordsHour = 0;
        int iCurrHour = Integer.parseInt(currentHour);

        for(WeatherData data : weatherDataList) {
            currRecordsHour = Integer.parseInt(DateUtils.getHour(data.getStrDate()));

            if(iCurrHour < currRecordsHour && iCurrHour > prevHour) {
                return data;
            }
        }
        return null;
    }

    /**
     * Gets the weather icon url
     * @param imageIconName
     * @return
     */
    public static String getImageIconURL(String imageIconName) {
        return Global.WEATHER_ICON_URL + imageIconName + Global.IMAGE_EXTENSION;
    }

    /**
     * Gets the maximum and minimum temperature string
     * @param mainWeather
     * @return
     */
    public static String getMaxMinTemp(MainWeather mainWeather) {
        return convertKelvinToCelcius(mainWeather.getMaxTemp()) +
                WeatherApp.getAppContext().getString(R.string.temperature_unit) +
                "/" +
                convertKelvinToCelcius(mainWeather.getMinTemp()) +
                WeatherApp.getAppContext().getString(R.string.temperature_unit);
    }

    /**
     * Gets the temperature
     * @param mainWeather
     * @return
     */
    public static String getTemperature(MainWeather mainWeather) {
        return convertKelvinToCelcius(mainWeather.getTemperature()) +
                WeatherApp.getAppContext().getString(R.string.temperature_unit);
    }

    /**
     * Gets wind data
     * @param wind
     * @return
     */
    public static String getWind(Wind wind) {
        return " " + degreesToCompass(wind.getDeg()) +
                " " + wind.getSpeed() + WeatherApp.getAppContext().getString(R.string.wind_unit);
    }

    /**
     * Gets humidity data
     * @param mainWeather
     * @return
     */
    public static String getHumidity(MainWeather mainWeather) {
        return  " " + mainWeather.getHumidity() + WeatherApp.getAppContext().getString(R.string.humidity_unit);
    }

    /**
     * Gets the pressure data
     * @param mainWeather
     * @return
     */
    public static String getPressure(MainWeather mainWeather) {
        return " " + mainWeather.getPressure() + WeatherApp.getAppContext().getString(R.string.pressure_unit);
    }

    /**
     * Gets the list of forecast data for a selected date
     * @param forecastList
     * @param date
     * @return
     */
    public static ArrayList<WeatherData> getSpecificForecast(List<WeatherData> forecastList, String date) {
        // this list will contain the passed day's weather forecast
        Collection<WeatherData> collections = Collections2.filter(forecastList,
                new DateWiseWeatherPredicate(date, true));

        // use the current time, and get the weather data
        // that needs to be displayed for the current hour
        ArrayList<WeatherData> weatherDataList = new ArrayList<>();
        weatherDataList.addAll(collections);
        return weatherDataList;
    }

    /**
     * Converts Kelvin to Celcius
     * @param kelvin
     * @return
     */
    private static String convertKelvinToCelcius(double kelvin) {
        DecimalFormat formatter = new DecimalFormat("#.##");
        return formatter.format(kelvin - Global.KELVIN_DIFFERENCE);
    }

    /**
     * Converts wind degrees to the wind direction
     * @param windDegrees
     * @return
     */
    private static String degreesToCompass(float windDegrees) {
        int val = (int)((windDegrees/22.5)+0.5);
        return windDirectionArr[(val % 16)];
    }
}
