package com.dhara.weatherapp.utils;

import com.dhara.weatherapp.constants.Global;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Dhara Shah on 17-05-2016.
 * Utility class for all date related operations
 */
public class DateUtils {
    /**
     * Gets the current date
     * @return
     */
    public static String getCurrentDate() {
        SimpleDateFormat output = new SimpleDateFormat(Global.DATE_FORMAT_YYYY_MM_DD,
                Locale.ENGLISH);
        return output.format(new Date());
    }

    /**
     * Gets the current hour
     * @return
     */
    public static String getCurrentHour() {
        SimpleDateFormat sdf = new SimpleDateFormat(Global.TIME_FORMAT_HH,
                Locale.ENGLISH);
        return sdf.format(new Date());
    }

    /**
     * Gets the day of the week
     * @param dateTime
     * @return
     */
    public static String getDay(String dateTime) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(Global.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS,
                    Locale.ENGLISH);
            SimpleDateFormat sdfOutput = new SimpleDateFormat(Global.DATE_FORMAT_EEE,
                    Locale.ENGLISH);
            Date dtTime = sdf.parse(dateTime);
            return sdfOutput.format(dtTime);
        }catch (ParseException e) {
            e.printStackTrace();
        }
        return dateTime;
    }

    /**
     * Gets the date in the form of yyyy-MM-dd
     * @param dateTime
     * @return
     */
    public static String getDate(String dateTime) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(Global.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS,
                    Locale.ENGLISH);
            SimpleDateFormat sdfOutput = new SimpleDateFormat(Global.DATE_FORMAT_YYYY_MM_DD,
                    Locale.ENGLISH);
            Date dtTime = sdf.parse(dateTime);
            return sdfOutput.format(dtTime);
        }catch (ParseException e) {
            e.printStackTrace();
        }
        return dateTime;
    }

    /**
     * Gets the time in the form HH:mm
     * @param dateTime
     * @return
     */
    public static String getTime(String dateTime) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(Global.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS,
                    Locale.ENGLISH);
            SimpleDateFormat sdfOutput = new SimpleDateFormat(Global.TIME_FORMAT_HH_MM,
                    Locale.ENGLISH);
            Date dtTime = sdf.parse(dateTime);
            return sdfOutput.format(dtTime);
        }catch (ParseException e) {
            e.printStackTrace();
        }
        return dateTime;
    }

    /**
     * Gets the hour from the date time passed
     * @param dateTime
     * @return
     */
    public static String getHour(String dateTime) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(Global.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS,
                    Locale.ENGLISH);
            SimpleDateFormat sdfOutput = new SimpleDateFormat(Global.TIME_FORMAT_HH,
                    Locale.ENGLISH);
            Date dtTime = sdf.parse(dateTime);
            return sdfOutput.format(dtTime);
        }catch (ParseException e) {
            e.printStackTrace();
        }
        return dateTime;
    }

    /**
     * Gets the date and time in milliseconds
     * @param dateTime
     * @return
     */
    public static long getDateTime(String dateTime) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(Global.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS,
                    Locale.ENGLISH);
            Date dtTime = sdf.parse(dateTime);
            return dtTime.getTime();
        }catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
