package com.dhara.weatherapp.utils.interfaces;

import com.dhara.weatherapp.models.Forecast;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Dhara Shah on 16-05-2016. </br>
 * Holds all the webservice calls to be made.
 */
public interface ApiService {
    /**
     * Performs weather search based on the city id
     * @param appId
     * @param cityId
     * @return
     */
    @GET("data/2.5/forecast?mode=json")
    Call<Forecast> getWeatherForecast(@Query(value = "appid", encoded = true) String appId,
                                      @Query(value = "id", encoded = true) long cityId);

    /**
     * Performs weather search based on the city name, incase, there is no
     * city id associated with that city
     * @param appId
     * @param cityCountryName
     * @return
     */
    @GET("data/2.5/forecast?mode=json")
    Call<Forecast> getWeatherForecast(@Query(value = "appid", encoded = true) String appId,
                                      @Query(value = "q", encoded = true) String cityCountryName);
}