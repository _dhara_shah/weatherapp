package com.dhara.weatherapp.utils.network;

import com.dhara.weatherapp.constants.Global;
import com.dhara.weatherapp.models.Forecast;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by OPTLPTP125 on 17-05-2016.
 */
public class ApiCalls {
    private static ApiCalls mApiCalls;

    /**
     * Creates a singleton
     * @return
     */
    public static ApiCalls getInstance(){
        if(mApiCalls == null) {
            mApiCalls = new ApiCalls();
        }
        return mApiCalls;
    }

    /**
     * Weather forecast call based on the city id
     * @param cityId
     * @param callback
     */
    public void getWeatherForecast(long cityId ,Callback<Forecast> callback) {
        // make the webservice call here
        Call<Forecast> call = new RestClient().getApiService().getWeatherForecast(Global.WEATHER_API_KEY,
                cityId);
        call.enqueue(callback);
    }

    /**
     * Weather forecast call based on the city name
     * @param cityCountryName
     * @param callback
     */
    public void getWeatherForecast(String cityCountryName ,Callback<Forecast> callback) {
        // make the webservice call here
        Call<Forecast> call = new RestClient().getApiService().getWeatherForecast(Global.WEATHER_API_KEY,
                cityCountryName);
        call.enqueue(callback);
    }
}
