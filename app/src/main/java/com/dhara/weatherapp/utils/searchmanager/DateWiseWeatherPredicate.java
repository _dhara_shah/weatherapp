package com.dhara.weatherapp.utils.searchmanager;

import com.dhara.weatherapp.models.WeatherData;
import com.google.common.base.Predicate;

/**
 * Created by Dhara Shah on 17-05-2016.
 */
public class DateWiseWeatherPredicate implements Predicate<WeatherData>{
    private String mCurrentDate;
    private boolean mIsCurrentDate;

    public DateWiseWeatherPredicate(String currentDate, boolean isCurrentDate) {
        mCurrentDate = currentDate;
        mIsCurrentDate = isCurrentDate;
    }

    @Override
    public boolean apply(WeatherData weatherData) {
        // gets all the objects not having the date passed
        if(!mIsCurrentDate && !weatherData.getStrDate().contains(mCurrentDate)) {
            return true;
        }else if(mIsCurrentDate && weatherData.getStrDate().contains(mCurrentDate) ) {
            // gets all the objects having the date passed
            return true;
        }
        return false;
    }
}
