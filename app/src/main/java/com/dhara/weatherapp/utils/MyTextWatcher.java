package com.dhara.weatherapp.utils;

import android.text.Editable;
import android.text.TextWatcher;

import com.dhara.weatherapp.interfaces.OnTextChangedListener;

/**
 * Created by Dhara Shah on 17-05-2016.
 */
public class MyTextWatcher implements TextWatcher {
    private OnTextChangedListener mListener;

    public MyTextWatcher(OnTextChangedListener listener) {
        mListener = listener;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if(mListener != null) {
            mListener.onTextChanged(s);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
