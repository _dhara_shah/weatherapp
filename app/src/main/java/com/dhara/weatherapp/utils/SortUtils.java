package com.dhara.weatherapp.utils;

import com.dhara.weatherapp.models.WeatherData;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Dhara Shah on 17-05-2016.
 */
public class SortUtils {
    /**
     * Enum for sorting the collections
     */
    public enum SortUtilsEnum {
        SORT_DATE_WISE,
        SORT_TEMP_WISE;
    }

    private static SortUtils mSortUtils;

    /**
     * Returns an instance of SortUtils class
     * @return
     */
    public static SortUtils getInstance() {
        if(mSortUtils == null) {
            mSortUtils = new SortUtils();
        }
        return mSortUtils;
    }

    /**
     * Performs a sort on the collections
     * @param weatherDataList
     * @param enumValue
     */
    public void sortObjects(List<WeatherData> weatherDataList,SortUtilsEnum enumValue ) {
        switch (enumValue) {
            case SORT_TEMP_WISE:
                Collections.sort(weatherDataList, new Comparator<WeatherData>() {
                    @Override
                    public int compare(WeatherData lhs, WeatherData rhs) {
                        if (lhs.getMain().getTemperature() > rhs.getMain().getTemperature()) {
                            return 1;
                        }else if (lhs.getMain().getTemperature() < rhs.getMain().getTemperature()) {
                            return -1;
                        }else {
                            return 0;
                        }
                    }
                });
                break;

            case SORT_DATE_WISE:
                Collections.sort(weatherDataList, new Comparator<WeatherData>() {
                    @Override
                    public int compare(WeatherData lhs, WeatherData rhs) {
                        long lhsTime =DateUtils.getDateTime(lhs.getStrDate());
                        long rhsTime = DateUtils.getDateTime(rhs.getStrDate());
                        if(lhsTime > rhsTime) {
                            return 1;
                        }else if (lhsTime < rhsTime) {
                            return -1;
                        }else {
                            return 0;
                        }
                    }
                });
                break;
        }
    }
}
