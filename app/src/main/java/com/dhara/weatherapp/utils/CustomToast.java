package com.dhara.weatherapp.utils;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.dhara.weatherapp.R;
import com.dhara.weatherapp.WeatherApp;

/**
 * Created by Dhara Shah on 18-05-2016.
 */
public class CustomToast {
    /**
     * Displays a custom toast message
     * @param message
     */
    public static void showToast(String message) {
        View view = LayoutInflater.from(WeatherApp.getAppContext()).inflate(R.layout.layout_toast, null);
        Toast toast = new Toast(WeatherApp.getAppContext());
        toast.setView(view);
        toast.setGravity(Gravity.CENTER, 0,0);

        TextView txtMessage = (TextView)view.findViewById(R.id.txt_message);
        txtMessage.setText(message);

        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }
}
