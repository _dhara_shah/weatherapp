package com.dhara.weatherapp.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.dhara.weatherapp.R;
import com.dhara.weatherapp.fragments.MainFragment;
import com.dhara.weatherapp.fragments.WeatherDetailFragment;
import com.dhara.weatherapp.models.WeatherData;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity {
    private Toolbar mToolbar;
    private MainFragment mMainFragment;
    private boolean mIsDetailView;

    @Override
    public void onBackPressed() {
        if(mIsDetailView) {
            // remove the current fragment
            // and replace it with the previous one
            replaceFragment(mMainFragment);
            setActionBarIcons(false);
            mIsDetailView = false;
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initToolbar();
        initFragment();
    }

    /**
     * Initializes the toolbar
     */
    private void initToolbar() {
        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    /**
     * Initializes the fragment
     */
    private void initFragment() {
        mMainFragment = MainFragment.newInstance();
        FragmentTransaction ft= getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame_container, mMainFragment);
        ft.commit();
    }

    /**
     * Displays the detail fragment
     * @param weatherDataList
     */
    public void replaceFragment(ArrayList<WeatherData> weatherDataList) {
        mIsDetailView = true;
        replaceFragment(WeatherDetailFragment.newInstance(weatherDataList));
        setActionBarIcons(true);
    }

    /**
     * Replaces a fragment
     * @param fragment
     */
    public void replaceFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame_container, fragment);
        ft.commit();
    }

    /**
     * Sets the navigation icon based on the fragment that would be diisplayed
     * @param isEnable
     */
    private void setActionBarIcons(boolean isEnable) {
        getSupportActionBar().setDisplayShowHomeEnabled(isEnable);
        getSupportActionBar().setDisplayHomeAsUpEnabled(isEnable);
    }
}
