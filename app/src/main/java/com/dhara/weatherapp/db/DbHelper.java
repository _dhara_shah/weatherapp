package com.dhara.weatherapp.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by OPTLPTP125 on 17-05-2016.
 */
public class DbHelper extends SQLiteOpenHelper {
    private static DbHelper mDatabaseHelper;
    private static SQLiteDatabase mSqliteDb;
    private Context mContext;

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "citylist.sqlite";

    /**
     * Gets an instance of the helper class
     * @param context
     * @return
     */
    public static DbHelper getInstance(Context context) {
        if (mDatabaseHelper == null) {
            mDatabaseHelper = new DbHelper(context);
            if(mSqliteDb != null) {
                mSqliteDb.close();
            }
        }
        return mDatabaseHelper;
    }

    /**
     * Initializes the database used
     * @param context
     */
    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
        openDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // do nothing
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    /**
     * Checks if the database is open or not
     * @return
     */
    @Override
    public synchronized void close() {
        if(mSqliteDb != null) {
            mSqliteDb.close();
        }
        super.close();
    }

    /**
     * Open an existing database or
     * copy the database from assets if it doesnot exist
     * @return
     */
    public SQLiteDatabase openDatabase() {
        File databaseFile = new File("/data/data/"+
                mContext.getApplicationInfo().packageName + "/databases");
        File dbFile = mContext.getDatabasePath(DATABASE_NAME);

        if (!databaseFile.exists()){
            databaseFile.mkdir();
        }

        if (!dbFile.exists()) {
            try {
                this.getReadableDatabase();
                copyDatabase(dbFile);
            } catch (IOException e) {
                throw new RuntimeException("Error creating source database", e);
            }
        }

        mSqliteDb = SQLiteDatabase.openDatabase(dbFile.getPath(), null,
                SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READONLY|SQLiteDatabase.CREATE_IF_NECESSARY);
        return mSqliteDb;
    }

    /**
     * Copy database from assets folder into the memory
     * @param dbFile
     * @throws IOException
     */
    private void copyDatabase(File dbFile) throws IOException {
        InputStream is = mContext.getAssets().open(DATABASE_NAME);
        OutputStream os = new FileOutputStream(dbFile);

        byte[] buffer = new byte[1024];
        while (is.read(buffer) > 0) {
            os.write(buffer);
        }

        os.flush();
        os.close();
        is.close();
    }

    /**
     * Get the city list
     * @return
     */
    public Cursor getCityCursor() {
        String[] columnsToFetch = new String[]{
                CityContract.CityEntry._ID,
                CityContract.CityEntry.COLUMN_CITY_NAME,
                CityContract.CityEntry.COLUMN_COUNTRY_CODE};

        mSqliteDb = this.openDatabase();

        Cursor c = mSqliteDb.query(CityContract.CityEntry.TABLE_NAME,columnsToFetch,
                null, null,null,null, CityContract.CityEntry.COLUMN_CITY_NAME + " ASC ");
        return c;
    }

    /**
     * Get the city list based on the searched term
     * @return
     */
    public Cursor getCitySelection(String constraint) {
        String[] columnsToFetch = new String[]{
                CityContract.CityEntry._ID,
                CityContract.CityEntry.COLUMN_CITY_NAME,
                CityContract.CityEntry.COLUMN_COUNTRY_CODE};

        String[] args = new String[]{"%" + constraint + "%"};

        mSqliteDb = this.openDatabase();

        Cursor c = mSqliteDb.query(CityContract.CityEntry.TABLE_NAME,
                columnsToFetch, CityContract.CityEntry.COLUMN_CITY_NAME + " LIKE ? ",
                args,
                null,
                null,
                CityContract.CityEntry.COLUMN_CITY_NAME + " ASC ");
        return c;
    }
}
