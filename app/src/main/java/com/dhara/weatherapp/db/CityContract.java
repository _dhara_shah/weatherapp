package com.dhara.weatherapp.db;

import android.provider.BaseColumns;

/**
 * Created by OPTLPTP125 on 17-05-2016.
 */
public final class CityContract {
    public CityContract(){}

    public static abstract class CityEntry implements BaseColumns {
        public static final String TABLE_NAME="citylist";
        public static final String COLUMN_CITY_NAME="name";
        public static final String COLUMN_COUNTRY_CODE="countrycode";
    }
}
