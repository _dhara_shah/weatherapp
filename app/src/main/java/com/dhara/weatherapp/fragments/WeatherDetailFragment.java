package com.dhara.weatherapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dhara.weatherapp.R;
import com.dhara.weatherapp.WeatherApp;
import com.dhara.weatherapp.adapters.FullDayForecastAdapter;
import com.dhara.weatherapp.constants.Global;
import com.dhara.weatherapp.models.WeatherData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dhara Shah on 16-05-2016.
 */
public class WeatherDetailFragment extends Fragment {
    private static WeatherDetailFragment mFragment;
    private RecyclerView mRecylerViewForecast;
    private FullDayForecastAdapter mFullDayForecastAdapter;
    private List<WeatherData> mWeatherDataList;

    /**
     * Creates an instance of the fragment
     * @param weatherDataList
     * @return
     */
    public static WeatherDetailFragment newInstance(ArrayList<WeatherData> weatherDataList) {
        mFragment = new WeatherDetailFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(Global.INTENT_WEATHER_LIST, weatherDataList);
        mFragment.setArguments(args);
        return mFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_detail_weather, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // initializations of the view takes place here
        mRecylerViewForecast = (RecyclerView)view.findViewById(R.id.recyclerView);

        mRecylerViewForecast.setHasFixedSize(true);

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(WeatherApp.getAppContext(),
                LinearLayoutManager.VERTICAL, false);

        mRecylerViewForecast.setLayoutManager(layoutManager);

        mWeatherDataList = new ArrayList<>();
        mFullDayForecastAdapter = new FullDayForecastAdapter(mWeatherDataList);
        mRecylerViewForecast.setAdapter(mFullDayForecastAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // get data and display it
        if(getArguments() !=  null) {
            ArrayList<WeatherData> weatherDataArrayList =
                    getArguments().getParcelableArrayList(Global.INTENT_WEATHER_LIST);
            mWeatherDataList.addAll(weatherDataArrayList);
        }

        if(mFullDayForecastAdapter != null) {
            mFullDayForecastAdapter.setList(mWeatherDataList);
            mFullDayForecastAdapter.notifyDataSetChanged();
        }
    }
}
