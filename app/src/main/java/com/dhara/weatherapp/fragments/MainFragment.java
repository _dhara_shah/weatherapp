package com.dhara.weatherapp.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FilterQueryProvider;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dhara.weatherapp.R;
import com.dhara.weatherapp.WeatherApp;
import com.dhara.weatherapp.activities.MainActivity;
import com.dhara.weatherapp.adapters.CityCursorAdapter;
import com.dhara.weatherapp.adapters.ForecastAdapter;
import com.dhara.weatherapp.customviews.ItemDecoration;
import com.dhara.weatherapp.utils.CustomToast;
import com.dhara.weatherapp.db.CityContract;
import com.dhara.weatherapp.db.DbHelper;
import com.dhara.weatherapp.interfaces.OnItemClickListener;
import com.dhara.weatherapp.interfaces.OnTextChangedListener;
import com.dhara.weatherapp.models.Forecast;
import com.dhara.weatherapp.models.WeatherData;
import com.dhara.weatherapp.utils.CommonUtils;
import com.dhara.weatherapp.utils.DateUtils;
import com.dhara.weatherapp.utils.MyTextWatcher;
import com.dhara.weatherapp.utils.SortUtils;
import com.dhara.weatherapp.utils.WeatherForecastUtils;
import com.dhara.weatherapp.utils.network.ApiCalls;
import com.dhara.weatherapp.utils.network.RestClient;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dhara Shah on 16-05-2016.
 * http://openweathermap.org/forecast5#bulk - to get the details about data received
 */
public class MainFragment extends Fragment implements View.OnClickListener , Callback<Forecast>,
        AdapterView.OnItemClickListener, OnItemClickListener {
    private static final int UPDATE_FORECAST_LIST =1;
    private static MainFragment mFragment;
    private OnItemClickListener mOnItemClickListener;
    private MaterialAutoCompleteTextView mEditCityName;
    private TextView mTxtDegrees;
    private ImageView mImgWeatherIcon;
    private TextView mTxtDescription;
    private TextView mTxtMaxMinTemp;
    private TextView mTxtHumidity;
    private TextView mTxtPressure;
    private TextView mTxtWind;
    private LinearLayout mLnrPressure;
    private LinearLayout mLnrHumidity;
    private LinearLayout mLnrWind;
    private ProgressBar mProgressBar;
    private ImageView mImgSearch;
    private Callback<Forecast> mCallback;
    private Forecast mForecast;
    private RecyclerView mForecastRecyclerView;
    private CityCursorAdapter mCityAdapter;
    private ForecastAdapter mAdapter;
    private Cursor mCityCursor;
    private long mCityId = -1;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case UPDATE_FORECAST_LIST:
                    // refresh the list view to display the latest records
                    mAdapter.setForecastList(mForecast.getGroupedForecastList());
                    mAdapter.notifyDataSetChanged();
                    break;
            }
        }
    };

    public static MainFragment newInstance() {
        mFragment = new MainFragment();
        return mFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mOnItemClickListener = this;

        // view initializatiions
        mEditCityName = (MaterialAutoCompleteTextView)view.findViewById(R.id.et_city_name);
        mTxtDegrees = (TextView)view.findViewById(R.id.txt_degrees);
        mTxtDescription = (TextView)view.findViewById(R.id.txt_weather_description);
        mTxtMaxMinTemp = (TextView)view.findViewById(R.id.txt_max_min_temp);
        mTxtHumidity = (TextView)view.findViewById(R.id.txt_humidity);
        mTxtWind = (TextView)view.findViewById(R.id.txt_wind);
        mTxtPressure = (TextView)view.findViewById(R.id.txt_pressure);
        mImgWeatherIcon = (ImageView)view.findViewById(R.id.img_current_weather_icon);
        mLnrPressure = (LinearLayout)view.findViewById(R.id.lnr_pressure);
        mLnrHumidity = (LinearLayout)view.findViewById(R.id.lnr_humidity);
        mLnrWind = (LinearLayout)view.findViewById(R.id.lnr_wind);

        mProgressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        mImgSearch = (ImageView)view.findViewById(R.id.img_search);

        mForecastRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        mForecastRecyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(WeatherApp.getAppContext(),
                LinearLayoutManager.HORIZONTAL, false);

        mForecastRecyclerView.setLayoutManager(layoutManager);
        mForecastRecyclerView.addItemDecoration(new ItemDecoration(getActivity()));

        List<WeatherData> weatherDataList = new ArrayList<>();
        if(mForecast != null) {
            weatherDataList.addAll(mForecast.getGroupedForecastList());
            setupData();
        }

        mAdapter = new ForecastAdapter(mOnItemClickListener);
        mAdapter.setForecastList(weatherDataList);
        mForecastRecyclerView.setAdapter(mAdapter);

        mEditCityName.setOnItemClickListener(this);
        mImgSearch.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mCallback = this;

        // gets the city list into the autocomplete text view
        mCityCursor = DbHelper.getInstance(WeatherApp.getAppContext()).getCityCursor();
        mCityAdapter = new CityCursorAdapter(WeatherApp.getAppContext(), mCityCursor, false);
        mEditCityName.setAdapter(mCityAdapter);

        mEditCityName.addTextChangedListener(new MyTextWatcher(new OnTextChangedListener() {
            @Override
            public void onTextChanged(CharSequence constraint) {
                if (mCityAdapter!=null) {
                    mCityAdapter.getFilter().filter(constraint);
                }
            }
        }));

        mCityAdapter.setFilterQueryProvider(new FilterQueryProvider() {
            public Cursor runQuery(CharSequence constraint) {
                Cursor cur = DbHelper.getInstance(WeatherApp.getAppContext())
                        .getCitySelection(constraint.toString());
                mCityCursor = cur;
                return cur;
            }

        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_search:
                validateCity();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Cursor c = mCityAdapter.getCursor();
        c.moveToPosition(position);

        mCityId = c.getLong(c.getColumnIndex(CityContract.CityEntry._ID));
    }

    @Override
    public void onStop() {
        super.onStop();
        mOnItemClickListener = null;
        if(mCityCursor != null) {
            mCityCursor.close();
        }
    }

    @Override
    public void onResponse(Call<Forecast> call, Response<Forecast> response) {
        mProgressBar.setVisibility(View.GONE);
        if(response.isSuccessful()) {
            // we have the response here
            // need to display it on the screen
            mForecast = response.body();

            if(mForecast.getForecastList() != null &&
                    !mForecast.getForecastList().isEmpty()) {
                setupData();
            }else {
                // not successful
                CustomToast.showToast(getString(R.string.error_msg_server_error));
            }
        }else {
            // not successful
            CustomToast.showToast(getString(R.string.error_msg_server_error));
        }
    }

    @Override
    public void onFailure(Call<Forecast> call, Throwable t) {
        mProgressBar.setVisibility(View.GONE);
        CustomToast.showToast(t.getMessage());
    }

    /**
     * Checks if the city name has been entered
     * and performs a request accordingly. If the city entered is not in the list
     * then the city id will not be sent but a search will be made based on the city name
     */
    private void validateCity() {
        if(TextUtils.isEmpty(mEditCityName.getText().toString())){
            mEditCityName.setError(getString(R.string.error_msg_enter_city_name));
        }else {
            if(RestClient.isNetworkAvailable()) {
                mProgressBar.setVisibility(View.VISIBLE);
                if(mCityId == -1) {
                    ApiCalls.getInstance().getWeatherForecast(mEditCityName.getText().toString(), mCallback);
                }else {
                    ApiCalls.getInstance().getWeatherForecast(mCityId, mCallback);
                }
            }else {
                // no internet available
                CustomToast.showToast(getString(R.string.error_msg_no_internet));
            }
        }

        View view = getActivity().getCurrentFocus();
        CommonUtils.hideKeyboard(view);
    }

    private void setupData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                // sort the data temperature wise
                SortUtils.getInstance().sortObjects(mForecast.getForecastList(),
                        SortUtils.SortUtilsEnum.SORT_TEMP_WISE);
                mForecast.setGroupedForecastList(WeatherForecastUtils.getWeatherList(mForecast.getForecastList()));
                mHandler.sendEmptyMessage(UPDATE_FORECAST_LIST);
            }
        }).start();

        // fetch today's data based on the time too
        // and display it

        WeatherData data = WeatherForecastUtils.getCurrentWeather(mForecast.getForecastList());
        if(data != null) {
            Glide.with(this).load(WeatherForecastUtils.getImageIconURL(data.getWeather().get(0).getIcon())).into(mImgWeatherIcon);

            mTxtDescription.setText(data.getWeather().get(0).getDescription());
            mTxtHumidity.setText(WeatherForecastUtils.getHumidity(data.getMain()));
            mTxtPressure.setText(WeatherForecastUtils.getPressure(data.getMain()));

            mLnrHumidity.setVisibility(View.VISIBLE);
            mLnrPressure.setVisibility(View.VISIBLE);

            if(data.getWind() != null) {
                mLnrWind.setVisibility(View.VISIBLE);
                mTxtWind.setText(WeatherForecastUtils.getWind(data.getWind()));
            }

            mTxtDegrees.setText(WeatherForecastUtils.getTemperature(data.getMain()));
            mTxtMaxMinTemp.setText(WeatherForecastUtils.getMaxMinTemp(data.getMain()));
        }
    }

    @Override
    public void onItemClick(WeatherData weatherData) {
        // get the date here and send the appropriate bunch of data
        // to replace the fragment

        String date = DateUtils.getDate(weatherData.getStrDate());
        ArrayList<WeatherData> weatherDataList =
                WeatherForecastUtils.getSpecificForecast(mForecast.getForecastList(),
                        date);

        ((MainActivity)getActivity()).replaceFragment(weatherDataList);
    }
}
