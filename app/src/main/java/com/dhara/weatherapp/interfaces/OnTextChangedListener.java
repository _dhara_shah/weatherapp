package com.dhara.weatherapp.interfaces;

/**
 * Created by Dhara Shah on 17-05-2016.
 */
public interface OnTextChangedListener {
    /**
     * Callback method for detecting text changes in the edit text
     * @param constraint
     */
    void onTextChanged(CharSequence constraint);
}
