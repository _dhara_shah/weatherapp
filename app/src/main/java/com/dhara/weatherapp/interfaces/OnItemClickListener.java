package com.dhara.weatherapp.interfaces;

import com.dhara.weatherapp.models.WeatherData;

/**
 * Created by Dhara Shah on 17-05-2016.
 */
public interface OnItemClickListener {
    /**
     * callback method for detecting an item click in the recycler view
     * @param weatherData
     */
    void onItemClick(WeatherData weatherData);
}
