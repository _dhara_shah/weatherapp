package com.dhara.weatherapp.constants;

/**
 * Created by Dhara Shah on 16-05-2016. </br>
 * Holds all the common constants used in the application
 */
public class Global {
    public static final String WEATHER_API_KEY = "cd63c47f27a407077a8d1e9b0ead60f6";
    public static final String DATE_FORMAT_YYYY_MM_DD="yyyy-MM-dd";
    public static final String DATE_FORMAT_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String TIME_FORMAT_HH="HH";
    public static final String TIME_FORMAT_HH_MM="HH:mm";
    public static final String DATE_FORMAT_EEE = "EEE";
    public static final double KELVIN_DIFFERENCE=273.15;
    public static final String IMAGE_EXTENSION=".png";
    public static final String WEATHER_ICON_URL = "http://openweathermap.org/img/w/";

    public static final String INTENT_WEATHER_LIST="weather_list";
}
