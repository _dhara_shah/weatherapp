package com.dhara.weatherapp;

import android.app.Application;
import android.content.Context;

/**
 * Created by Dhara Shah on 16-05-2016.
 */
public class WeatherApp extends Application {
    private static WeatherApp mApp;
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mApp = this;
        mContext = this;
    }

    /**
     * Gets an instance of the application context
     * @return
     */
    public static WeatherApp getAppContext() {
        if(mApp == null) {
            mApp = (WeatherApp)mContext;
        }
        return mApp;
    }
}
