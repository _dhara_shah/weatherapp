package com.dhara.weatherapp.activitytests;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.dhara.weatherapp.R;
import com.dhara.weatherapp.activities.MainActivity;
import com.dhara.weatherapp.utils.MyViewAction;
import com.dhara.weatherapp.utils.RecyclerViewMatcher;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.hasErrorText;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

/**
 * Created by Dhara Shah on 16-05-2016.
 */
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {
    @Rule
    public ActivityTestRule<MainActivity> mMainActivityRule =
            new ActivityTestRule<>(MainActivity.class,true, true);

    @Test
    public void testEmptyCitySearch() throws InterruptedException {
        onView(withId(R.id.img_search)).perform(click());
        onView(withId(R.id.et_city_name))
                .check(matches(hasErrorText(mMainActivityRule.getActivity().getString(R.string.error_msg_enter_city_name))));
        Thread.sleep(1000);
    }

    @Test
    public void testGetWrongCityWeatherForecast() throws InterruptedException {
        onView(withId(R.id.et_city_name)).perform(typeText("."),
                closeSoftKeyboard());
        onView(withId(R.id.img_search)).perform(click());
        onView(withText(R.string.error_msg_server_error))
                .inRoot(withDecorView(not(is(mMainActivityRule.getActivity().getWindow().getDecorView()))))
                .check(matches(isDisplayed()));
        Thread.sleep(1000);
    }

    @Test
    public void testGetWeatherForecast() throws InterruptedException {
        onView(withId(R.id.et_city_name))
                .perform(typeText("Berlin,DE"));
        onView(withId(R.id.img_search)).perform(click());
        Thread.sleep(2000);
        onView(withId(R.id.lnr_humidity)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    @Test
    public void testForecastList() throws InterruptedException {
        onView(withId(R.id.et_city_name))
                .perform(typeText("Berlin,DE"));
        onView(withId(R.id.img_search)).perform(click());
        Thread.sleep(2000);

        onView(withId(R.id.recyclerView)).perform(
                RecyclerViewActions.actionOnItemAtPosition(0,
                        MyViewAction.clickChildViewWithId(R.id.lnr_root)));

        onView(new RecyclerViewMatcher(R.id.recyclerView)
                .atPositionOnView(0, R.id.txt_day))
                .check(matches(isDisplayed()));
    }
}
