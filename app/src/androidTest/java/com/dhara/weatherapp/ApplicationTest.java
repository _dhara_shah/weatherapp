package com.dhara.weatherapp;

import android.test.ApplicationTestCase;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<WeatherApp> {
    private WeatherApp mApp;
    public ApplicationTest() {
        super(WeatherApp.class);
    }

    protected void setUp() throws Exception {
        super.setUp();
        createApplication();
        mApp = getApplication();
        assertNotNull(mApp);
    }
}