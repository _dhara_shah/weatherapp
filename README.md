# About the application #
WeatherApp allows users to get weather associated to a city either by selecting from the dropdown available or entering a city yourself.

# Features of the application #
- Search weather based on the city entered
- Get the current weather as per the current time
- Get the list of forecast for the next 5 days

OpenWeatherApi is used in order to fetch the weather information

# Supported devices #
This application works on all smart-phones excluding tablets for now

# How to get setup #
- Download android studio (any version will do, preferably the new version)
- Clone this repository or download it and import the project like any other android project
- Run the application
- You will get to see the apk at the location app/build/output/apk/

# Dependencies #
There are no extra modules required for this application.

The common gradle dependencies used in this application include:
- Appcompat
- Recycler view
- Android unit test dependencies
- Espresso
- Retrofit and okHttp

# Test cases #
Android Junit4 instrumentation test cases have been written and Espresso is used for UI Automated test cases

# Known issues or shortcomings #
- OpenWeatherApi is free for only a 5 day weather forecast, hence the 7day weather forecast could not be implemented
- The android test case does not cover the scenario of clicking a value from the autocomplete text view dropdown
- The city list is obtained from an sqlite file - data obtained from http://openweathermap.org/help/city_list.txt

# Contact #
In case of any query you can contact *sdhara2@hotmail.com*